'use strict';

// require the Mandelbrot theme module
const mandelbrot = require('@frctl/mandelbrot');

// create a new instance with custom config options
const myCustomisedTheme = mandelbrot({
	skin: 'red',
});

/* Create a new Fractal instance and export it for use elsewhere if required */
const fractal = (module.exports = require('@frctl/fractal').create());

/* Set the title of the project */
fractal.set('project.title', 'SantanderCGS');
fractal.set('project.author', 'Alejandro Mur');

/* Tell Fractal where the components will live */
fractal.components.set('path', __dirname + '/src/components');

/* Tell Fractal default preview layout */
fractal.components.set('default.preview', '@preview');

/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/src/docs');

// Tell Fractal where static files will live
fractal.web.set('static.path', __dirname + '/public');

// Prefix the URL path of the assets
// fractal.web.set('static.mount', '/public/');

// Tell Fractal where build destination will live
fractal.web.set('builder.dest', __dirname + '/build');

const hbs = require('@frctl/handlebars')({
	/* other configuration options here */
});

fractal.components.engine(
	hbs
); /* set as the default template engine for components */
fractal.docs.engine(
	hbs
); /* you can also use the same instance for documentation, if you like! */

// tell Fractal to use the configured theme by default
fractal.web.theme(myCustomisedTheme);

const instance = fractal.components.engine(hbs);

instance.handlebars.registerHelper('switch', function (value, options) {
	this.switch_value = value;
	return options.fn(this);
});

instance.handlebars.registerHelper('case', function (value, options) {
	if (value == this.switch_value) {
		return options.fn(this);
	}
});

instance.handlebars.registerHelper('default', function (value, options) {
	return true; ///We can add condition if needs
});

instance.handlebars.registerHelper('ifZero', function (index, options) {
	if (index === 0) {
		return options.fn(this);
	} else {
		return options.inverse(this);
	}
});
