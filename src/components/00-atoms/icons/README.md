# Set de iconos

Ésta es la previsualización del set de iconos

# Utilización

Para utilizar cualquier icono dentro de un documento HTML hay que incluirlo a través de una etiqueta `<svg>` con una etiqueta `<use>` apuntando a la `id` definida para ese icono, como se ve en el ejemplo.

# Mantenimiento

Cuando se quiera añadir o quitar un icono del set, porfavor actualizar éste componente
