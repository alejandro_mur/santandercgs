class NavMobile {
  constructor() {
    this.$body = $('body');
    this.$menuButton = $('[data-menu-role="trigger-menu"]');
    this.$menuNav = $('[data-menu-role="nav"]');
    this.isOpen = false;
    this.classes = {
      menu: 'is-open',
      body: 'nav-is-open',
    };
  }

  bindHandlers() {
    this.$menuButton.on('click', () => this.toggleMenu());
  }

  toggleMenu() {
    this.$menuNav.toggleClass(this.classes.menu);
    this.$body.toggleClass(this.classes.body);
    this.isOpen = !this.isOpen;
  }

  init() {
    this.bindHandlers();
  }
}

if ($('[data-role="menu"]').length) {
  const navMobile = new NavMobile();
  navMobile.init();
}
