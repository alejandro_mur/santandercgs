$.fn.isInViewport = function () {
  let elementTop = $(this).offset().top;
  let elementBottom = elementTop + $(this).outerHeight();

  let viewportTop = $(window).scrollTop();
  let viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

class Counter extends CountUp {
  constructor(el) {
    this.$el = $(el);
    this.limit = this.$el.attr('[data-counter-limit]');
    this.$counter = this.$el.find('[data-counter-role="num"]');
  }

  init() {
    this.$el[0].start();
  }
}

if ($('[data-role="counter"]').length) {
  let counters = [];

  $('[data-role="counter"]').each((i, el) => {
    let id = $(el).attr('id');
    let limit = $(el).attr('data-counter-limit');
    let duration = $(el).attr('data-counter-duration');
    let counter = new CountUp(id, 0, limit, 0, duration, {
      separator: '.',
    });
    counters.push(counter);
  });

  $(window).on('scroll', () => {
    if ($('[data-role="counters"]').isInViewport()) {
      console.log('success.');
      $(counters).each((idx, counter) => counter.start());
    }
  });
}
