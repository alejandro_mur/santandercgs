class Video {
  constructor(el) {
    this.$el = $(el);
    this.$buttonPlayer = $('[data-video-role="init-player"]');
    this.$video = $('[data-video-role="video"]');
    this.isRunning = false;
    this.classes = {
      buttonPlayer: 'd-none',
    };
  }

  bindHandlers() {
    this.$buttonPlayer.on('click', () => this.attachControls());
  }

  attachControls() {
    this.$buttonPlayer.addClass(this.classes.buttonPlayer);
    this.$video[0].play();
    this.$video.attr('controls', true);
    this.isRunning = !this.isRunning;
  }

  checkAndPauseVideo() {
    console.log(this.isRunning);
    if (this.isRunning) {
      this.$video[0].pause();
    }
  }
}

function setupVideosAndSlider() {
  const $slider = new Flickity('.main-slider', {
    wrapAround: true,
    adaptiveHeight: true,
  });

  $('.slide-video').each((i, el) => {
    let player = new Video(el);
    player.bindHandlers();
    playerInstances.push(player);
  });

  $slider.on('change', function (index) {
    console.log('changing');
    playerInstances[playerInstanceRunning].checkAndPauseVideo();
  });
}

let playerInstances = [];
let playerInstanceRunning = 0;

if (
  $('.main-slider').length &&
  window['Flickity'] &&
  $('[data-role="video"]').length
) {
  setupVideosAndSlider();
}
