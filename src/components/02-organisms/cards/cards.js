const CARDS_DATA = [
  {
    id: 1,
    url: '#',
    src: '/images/present-1.jpg',
    date: '02/11/2021',
    title: 'Adapta tu negocio a la pandemia',
  },
  {
    id: 2,
    url: '#',
    src: '/images/present-3.jpg',
    date: '02/10/2021',
    title: 'Plataformas de pago online seguras',
  },
  {
    id: 3,
    url: '#',
    src: '/images/present-2.jpg',
    date: '12/11/2021',
    title: 'Teletrabajo y conciliación familiar',
  },
];

let cardTpl = (data) => {
  return data.map((itemData, idx) => {
    const { src, date, title, url } = itemData;
    return `
      <article class="card" data-aos="fade-up" data-aos-duration="400" data-aos-delay="${
        100 * idx
      }">
        <header class="card-header">
          <img width="325" height="250" src="${src}" alt="" class="card__img">
        </header>
        <div class="card-body">
            <time class="card__date" datetime="${date}">${date}</time>
          <h2 class="card__heading">${title}</h2>
          <a class="card__link" href="${url}">
            <span>Leer artículo</span>
            <svg width="6" height="10" viewBox="0 0 28 48">
              <use xlink:href="#arrow"></use>
            </svg>
          </a>
        </div>
      </article>
    `;
  });
};

class LoadMore {
  constructor(el) {
    this.$el = $(el);
    this.$region = $('[data-role="cards-region"]');
  }

  bindHandlers() {
    this.$el.on('click', () => this.requestForData());
  }

  lockButton() {
    this.$el.prop('disabled', true);
  }

  releaseButton() {
    this.$el.prop('disabled', false);
  }

  // Mock AJAX / FETCH
  requestForData() {
    this.lockButton();

    $.ajax({
      method: 'GET',
      url: 'https://jsonplaceholder.typicode.com/todos/1',
      success: (data) => {
        this.fillItemTemplate(data);
        this.releaseButton();
      },
      error: (error) => {
        console.log(error);
      },
    });

    // fetch('https://api.mocki.io/v1/b043df5a')
    //   .then((response) => response.json())
    //   .then((json) => this.fillItemTemplate(json));
  }

  fillItemTemplate(data) {
    data = CARDS_DATA; // (1) Replace DATA with mock data

    const items = cardTpl(data);
    this.appendToRegion(items);
  }

  appendToRegion(data = '<p>No hay más entradas disponibles</p>') {
    this.$region.append(data);
  }
}

if ($('[data-role="load-more"]').length) {
  const loadMore = new LoadMore('[data-role="load-more"]');
  loadMore.bindHandlers();
}
