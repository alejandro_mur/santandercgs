# Santander CGS

Aplicación generada con [Fractal](http://fractal.build) como herramienta para crear la librería de componentes que nos servirán a su vez para crear templates, mantener y evolucionar la aplicación.

## Desarrollo

Tanto si vamos realizar modificaciones en los estáticos **HTML, CSS, JS o IMG** como si vamos a crear nuevos componentes.

- Para instalar las dependencias del proyecto ejecutar `npm install`
- Para empezar a desarrollar ejecutar `gulp`
- Para generar la versión para producción ejecutar `gulp build`

### Estructura

```
|-- build/
|-- src/
    |-- assets/
    |-- components/
    |-- docs/
```

## Recursos
